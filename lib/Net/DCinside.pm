package Net::DCinside;
use utf8;
use strict;
use warnings;
use WWW::Mechanize;
use HTTP::Cookies;
use HTML::TreeBuilder;

our $VERSION="0.001";

sub new {
    my $class = shift;
    my $self = {};
    $self->{user_id} = $_[0];
    $self->{password} = $_[1];
    $self->{cookies} = HTTP::Cookies->new;
    bless($self, $class);
  
  return $self;
}

sub login {
  my $self = shift;
  my ($user_id, $password) = @_;
  if (!@_){
    $user_id = $self->{user_id};
    $password = $self->{password};
  }

  my $mech = WWW::Mechanize->new( cookie_jar => $self->{cookies} );
  $mech->add_header( Origin => 'http://dcid.dcinside.com', Referer => 'http://dcid.dcinside.com/join/login.php' );
  
  my $res = $mech->post('https://dcid.dcinside.com/join/member_check.php', { 
      user_id => $user_id, 
      password => $password, 
      ssl_chk => 'on', 
  });
  
  unless ($res->is_success) {
    warn "Failed to Connect dcinside.com";
    return;
  }
  
  #로그인 성공 시 인증 url이 담긴 iframe을 문서 마지막에 뙇하고 넘겨줌.
  my $tree = HTML::TreeBuilder->new->parse($res->decoded_content);
  eval{ 
    my $authorization_url = $tree->look_down("_tag", "iframe")->attr('src');
    my $res = $mech->get($authorization_url); 
    return unless $res->is_success;
  };
  
  if ($@) {
    warn $@;
    return;
  }
  
  return 1;
}

sub as_cookiejar {
  my $self = shift;
  return unless $self;
  return $self->{cookies};
}

sub write {
  my $self = shift;
  my $param = {@_};
  my $mech = WWW::Mechanize->new( cookie_jar => $self->{cookies} );
  
  
  my $res = $mech->get( sprintf('http://gall.dcinside.com/article_write.php?id=%s', $param->{gallery}) );
  
  unless ($res->is_success) {
    warn "Couldn't connect to gall.dcinside.com!";
    return;
  }
  
  my $tree = HTML::TreeBuilder->new->parse($res->decoded_content);
  my ($user_id, $ukey, $name, $password);
  if ($param->{guest}){
    $name = $param->{guest}->{name};
    $password = $param->{guest}->{password};
  }
  $user_id          = $tree->look_down("_tag", "input", "name", "user_id")->attr('value');
  $ukey             = $tree->look_down("_tag", "input", "name", "ukey")->attr('value');
  
  my $code          = $tree->look_down("_tag", "input", "name", "code")->attr('value');
  my $t_no	    = $tree->look_down("_tag", "input", "name", "t_no")->attr('value');
  my $gall_category = $tree->look_down("_tag", "input", "name", "gall_category")->attr('value');
  
  $res = $mech->post("http://upload.dcinside.com/g_write.php", {
      namecheck => 1,
      mode => 'write',
      id => $param->{gallery},
      
      #이 둘은 로그인시에만 사용함
      user_id => $user_id,
      ukey => $ukey,
      
      #유동닉의 경우에는 name과 password가 대신 들어감
      name => $name,
      password => $password,
      
      code => $code,
      t_no => $t_no,
      
      gall_category => $gall_category,
      subject => $param->{subject},
      memo    => $param->{body},
  });
    
  unless ($res->is_success) {
    #warn "";
    return;
  }
  
  $tree = HTML::TreeBuilder->new->parse($res->decoded_content);
  my $url;
  eval{
    my $meta = $tree->look_down('_tag', 'meta')->attr('content');
    $url = $1 if $meta =~ /url=(.+)/;
  };  
  
  if ($@) {
    return;
  }
  
  return $url;
    
  print $res->decoded_content if $res->is_success;
}




sub comment {
  my $self = shift;
  my $param = {@_};
  my $mech = WWW::Mechanize->new( cookie_jar => $self->{cookies} );
 
  my ($name, $password);
  
  if ($param->{guest}){
    $name = $param->{guest}->{name};
    $password = $param->{guest}->{password};
  }
  $mech->add_header( Origin => 'http://gall.dcinside.com', Referer => sprintf('http://gall.dcinside.com/list.php?id=%s&no=%d', $param->{gallery}, $param->{no}) );
  
  my $res = $mech->post('http://gall.dcinside.com/comment_ok.php', {
      rType => 'iframe',
      
      id => $param->{gallery},
      no => $param->{no},
      
      #유동닉인 경우 name과 password가 추가됨.
      name => $name,
      password => $password,
      
      memo => $param->{comment},
  });
  
  print $res->decoded_content;
  
  return unless $res->is_success;
  
  return 1;
}

1;
