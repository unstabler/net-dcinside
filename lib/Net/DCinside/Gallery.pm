package Net::DCinside::Gallery;
use utf8;
use strict;
use warnings;
use WWW::Mechanize;
use HTTP::Cookies;
use HTML::TreeBuilder;
use JSON;
use Encode qw/encode decode/;

sub new {
  my $class = shift;
  my $self = {};
  $self->{gallery_id} = $_[0];
  
  bless($self, $class);
  return $self;
}

sub list {
  my $self = shift;
  my $page = $_[0];
  $page = 1 unless $page;
  
  my $mech = WWW::Mechanize->new();

  my $res = $mech->get( sprintf('http://gall.dcinside.com/list.php?id=%s&page=%d', $self->{gallery_id},$page) );
  unless ($res->is_success) {
    warn "서버 접속에 실패했습니다.";
    return;
  }
  
  my $tree = HTML::TreeBuilder->new->parse($res->decoded_content);
  my @article_list = $tree->look_down("_tag", "tr", "onMouseOver", "this.style.backgroundColor='#F2F0F9'");
  my @list;
  for my $article (@article_list) {
    next if $article->look_down("_tag", "font")->as_text eq "공지";
    my $article_num = $article->look_down("_tag", "span", "style", "font-size:8pt;font-family:tahoma")->as_text;
    my $article_title = $article->look_down("_tag", "a")->as_text;
    my $article_name = $article->look_down("_tag", "a", "class", "btnName")->as_text;
    push @list, {
        num       => $article_num,
        title     => $article_title,
        name      => $article_name,
        user_type => 0, #음.. 고정닉 / 일반닉 / 유동닉 이렇게 세가지가 있고 와글도 있고 황금고정닉도 있고 젠장 어쩌지 
    }
  }
  
  return \@list;
}

sub ilbe {
  my $self = shift;
  my $day = $_[0];
  $day = 0 unless $day;
  
  if ($day > 7) {
    warn "7일 이상 전의 일베는 확인할 수 없습니다.";
    return;
  }
  
  my $mech = WWW::Mechanize->new();
  my $res = $mech->get( sprintf('http://json.dcinside.com/ilbe/ilbe_json/ilbe_%s_%d.php', $self->{gallery_id}, $day) );
  unless ($res->is_success) {
    warn "";
    return;
  }
  my $json = $res->decoded_content;
  $json =~ s/(^\()|(\)$)//g;
  
  return decode_json(encode('utf-8',$json));
}

sub search {

}


1;