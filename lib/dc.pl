#!/usr/bin/env perl
use 5.010;
use utf8;
use Net::DCinside;

my $dc = Net::DCinside->new();

my $result = $dc->comment(
    guest => { name => '테스트', password => '2345' },
    gallery => 'rhythmgame',
    no => 2689985,
    comment => '덧글달기 테스트',
);

print $result;